package Utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class Util {

    public static WebElement waitForGenericElement(WebDriver driver, By by, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        return wait.until(ExpectedConditions.
                presenceOfElementLocated(by));
    }

    public static String[] parseJson(String filename) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(new File(filename));
        String username = root.get("account").get("username").asText();
        String password = root.get("account").get("password").asText();
        String error = root.get("error").asText();
        return new String[]{username, password, error};
    }

    public static ArrayList<String[]> parseJsonArray(String filename) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootArray = mapper.readTree(new File(filename));
        ArrayList<String[]> jsonobjs = new ArrayList<>();
        for (JsonNode root : rootArray) {
            String username = root.get("account").get("username").asText();
            String password = root.get("account").get("password").asText();
            String error = root.get("error").asText();
            jsonobjs.add(new String[]{username, password, error});
        }
        return jsonobjs;
    }

    public static String getRandomString(int size) {
        final String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String results = "";
        for (int i = 0; i < size; i++) {
            Random rnd = new Random();
            results = results + alphabet.charAt(rnd.nextInt(alphabet.length()));
        }
        return results;
    }

    public static String getRandomEmail() {
        String user = getRandomString(10);
        String domain = "gmail";
        String tld = "com";
        StringBuilder sb = new StringBuilder();
        //System.out.println(user);
        return sb.append(user).append("@").append(domain).append(".").append(tld).toString();
    }

    public static void makeScreenshot(WebDriver driver, String testName) {

        DateFormat dateFormat = new SimpleDateFormat("HHmmss_yyyyMMdd");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
        String name = "screenshot_" + testName + "_" + dateFormat.format(date) + ".png";
        String resultsPath = System.getProperty("user.dir") + "/test-results/screenshots" + "/" + name;

        File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File finalFile = new File(resultsPath);
        try {
            FileUtils.copyFile(screenshotFile, finalFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<File> getListOfFiles(String path, String ext) {
        List<File> textFiles = new ArrayList<File>();
        File dir = new File(path);
        for (File file : dir.listFiles()) {
            if (file.getName().endsWith("." + ext)) {
                textFiles.add(file);
            }
        }
        return textFiles;
    }
}

