package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pageFactory.PcGarageMainPage;
import pageFactory.WhishList;

public class WishTest extends Base {

    @Test
    public void WishTest01() throws InterruptedException {
        driver.get(hostname+"/autentificare/");
        PcGarageMainPage auth = new PcGarageMainPage(driver);
        //AddItemToBasket additem = new AddItemToBasket();
        auth.loginPage("alex@gmail.com", "alex12345");
        WhishList wish = new WhishList(driver);
        wish.createWishList("test", "test12");
    }
}
