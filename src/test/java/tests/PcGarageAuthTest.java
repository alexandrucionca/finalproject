package tests;

import Utils.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageFactory.PcGarageMainPage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class PcGarageAuthTest extends Base {
    @DataProvider(name = "negativeData")
    public Iterator<Object[]> dataNegatives() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        // username, password, error, succes
        dp.add(new String[]{"dad@yahoo.com", "aaaa","Adresa de email nu exista in baza noastra de date. Va rugam sa incercati din nou."});
        dp.add(new String[]{"aaaaaa@gmail.com","aaaa","Parola introdusa este gresita. Va rugam sa incercati din nou."});
      //  dp.add(new String[]{"assda@yahoo.com", "aaaaa","Adresa de email nu exista in baza noastra de date. Va rugam sa incercati din nou."});
        return dp.iterator();
    }

    @DataProvider(name = "positiveData")
    public Iterator<Object[]> positiveData() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"alex@gmail.com", "alex12345","Setari site PC Garage"});
        return dp.iterator();
    }

    @Test
    public void authenticateTest01() {
        driver.get(hostname+"/autentificare/");
        PcGarageMainPage auth = new PcGarageMainPage(driver);
        auth.loginPage("abc@gmail.com", "abc123");
    }
    @Test(dataProvider = "positiveData")
    public void positiveAuthenticate(String username,String password,String success){
        driver.get(hostname+"/autentificare/");
        PcGarageMainPage auth = new PcGarageMainPage(driver);
        auth.loginPage(username,password);
        //Assert.assertTrue(auth.checkForMessage(successmessage,success));
        WebElement successmessage = waitForGenericElement(By.xpath("//*[@id=\"listing-right\"]/div[2]/p"), 1);
        Assert.assertEquals(success, successmessage.getText());
        System.out.println("User autentificat cu succes");

    }


    @Test(dataProvider = "negativeData")
    public void negativeAuthenticate(String username,String password,String error){
        driver.get(hostname+"/autentificare/");
        PcGarageMainPage auth = new PcGarageMainPage(driver);
        auth.loginPage(username,password);
        //Assert.assertTrue(auth.checkForMessage(errormessage,error));
        WebElement errormessage = waitForGenericElement(By.xpath("//*[@id=\"fixed-content-wrapper\"]/p"), 10);
        Assert.assertEquals(error, errormessage.getText());
        System.out.println("Adresa de email nu exista in baza noastra de date. Va rugam sa incercati din nou.");
    }

    @Test
    public void registerTest01() {
        driver.get(hostname+"/autentificare/");
        String username = Util.getRandomEmail();
        String password = Util.getRandomString(5);
        PcGarageMainPage auth = new PcGarageMainPage(driver);
        auth.register("ABC",
                "CDE",
                "0722222222",
                username,
                password);
    }

}
