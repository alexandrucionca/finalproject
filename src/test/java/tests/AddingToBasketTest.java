package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pageFactory.AddItemToBasket;
import pageFactory.PcGarageMainPage;

public class AddingToBasketTest extends  Base {
    @Test
    public void addItemToBasketTest01() {
        driver.get(hostname+"/autentificare/");
        PcGarageMainPage auth = new PcGarageMainPage(driver);
        AddItemToBasket additem=new AddItemToBasket(driver);
        auth.loginPage("alex@gmail.com", "alex12345");
        additem.checkBasket();
    }

}
