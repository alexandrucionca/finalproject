package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageFactory.PcGarageMainPage;

import Utils.Util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class PcGarageAuthTestJson extends Base {
    @DataProvider(name = "negativeDataJson")
    public Iterator<Object[]> dataNegatives() throws IOException {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        // username, password, error, succes
        ArrayList<String[]> arr = Util.parseJsonArray("C:\\Users\\lenovo\\Desktop\\jsonel\\negativeTesting.json");
        for (String[] a : arr) {
            dp.add(a);
        }
        return dp.iterator();
    }

    @DataProvider(name = "positiveDataJson")
    public Iterator<Object[]> positiveData() throws IOException {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        String[] pt = Util.parseJson("C:\\Users\\lenovo\\Desktop\\jsonel\\positiveTesting.json");
        dp.add(pt);
        return dp.iterator();
    }

    @Test(dataProvider = "positiveDataJson")
    public void positiveAuthenticate(String username,String password,String success){
        driver.get(hostname+"/autentificare/");
        PcGarageMainPage auth = new PcGarageMainPage(driver);
        auth.loginPage(username,password);
        //Assert.assertTrue(auth.checkForMessage(successmessage,success));
        WebElement successmessage = waitForGenericElement(By.xpath("//*[@id=\"listing-right\"]/div[2]/p"), 1);
        Assert.assertEquals(success, successmessage.getText());
        System.out.println("User autentificat cu succes");

    }


    @Test(dataProvider = "negativeDataJson")
    public void negativeAuthenticate(String username,String password,String error){
        driver.get(hostname+"/autentificare/");
        PcGarageMainPage auth = new PcGarageMainPage(driver);
        auth.loginPage(username,password);
        //Assert.assertTrue(auth.checkForMessage(errormessage,error));
        WebElement errormessage = waitForGenericElement(By.xpath("//*[@id=\"fixed-content-wrapper\"]/p"), 10);
        Assert.assertEquals(error, errormessage.getText());
        System.out.println("Adresa de email nu exista in baza noastra de date. Va rugam sa incercati din nou.");
    }

}
