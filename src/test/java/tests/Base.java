package tests;

import Utils.Util;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

import static Utils.SeleniumUtils.getDriver;

public class Base {
    WebDriver driver;
    //String hostname= "192.168.1.106";
    String hostname;
    String cookiesUrl;

    //    String browserType = "chrome";
//
    @BeforeClass
    public void beforeTest() {
        // Method 1 -D cmd line parameters
        System.out.println(System.getProperty("browserType"));

        // Method 2 OS Environment variable
        System.out.println("ENV BROWSER Value: " + System.getenv("browserType"));
        setUp();
    }
        // Method 3 Property file
        public void setUp(){
        if(driver==null)
        try {
            InputStream input = new FileInputStream("src\\test\\java\\framework.properties");
            Properties prop = new Properties();
            prop.load(input);
            driver = getDriver(prop.getProperty("browserType"));
            System.out.println("PROPERTY BROWSER Value: " + prop.getProperty("browserType"));
            hostname = prop.getProperty("hostname");
            System.out.println("PROPERTY Cookies URL Value: " + prop.getProperty("cookies.url"));
            cookiesUrl = prop.getProperty("cookies.url");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }


    @AfterTest(alwaysRun = true)
    public void closeBrowserAtEnd() {
        System.out.println("Close driver on AfterTest");
        driver.quit();
    }

    /**
     * This Method it is used to make screen shots any time when a test fail
     */
    @AfterMethod
    public void triggerScreenshoot(ITestResult result) {
        //using ITestResult.FAILURE is equals to result.getStatus then it enter into if condition
        if (ITestResult.FAILURE == result.getStatus() && ((RemoteWebDriver) driver).getSessionId() != null)
            Util.makeScreenshot(driver, result.getName());
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public WebElement waitForGenericElement(By by, int timeout) {
        return Util.waitForGenericElement(driver, by, timeout);
    }
}
