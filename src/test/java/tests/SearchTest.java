package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pageFactory.PcGarageMainPage;
import pageFactory.SearchFunctionality;

public class SearchTest extends Base {

    @Test
    public void searchTest01() {
        driver.get(hostname+"/autentificare/");
        PcGarageMainPage auth = new PcGarageMainPage(driver);
        //AddItemToBasket additem = new AddItemToBasket();
        auth.loginPage("alex@gmail.com", "alex12345");
        SearchFunctionality search = new SearchFunctionality(driver);
        search.searchFunction("Telefon mobil Nokia 3310 3G Dual SIM Charcoal (2017)");
    }
}
