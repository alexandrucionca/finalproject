package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class PcGarageMainPage {
    WebDriver driver;

    @FindBy(how = How.ID, using = "email")
    WebElement emailField;

    @FindBy(how = How.ID, using = "password")
    WebElement passwordField;

    @FindBy(how = How.CSS, using = "#login > div > div > button")
    WebElement submitButton;

    @FindBy(how = How.ID, using = "newfirstname")
    WebElement newfirstname;

    @FindBy(how = How.ID, using = "newlastname")
    WebElement newlastname;

    @FindBy(how = How.ID, using = "telephone")
    WebElement telephone;

    @FindBy(how = How.ID, using = "newemail")
    WebElement newemail;

    @FindBy(how = How.ID, using = "newpassword")
    WebElement newpassword;

    @FindBy(how = How.ID, using = "newpasswordretype")
    WebElement newpasswordretype;

    @FindBy(how = How.CSS, using = "#register > div > div > button")
    WebElement submitButtonRegister;
    @FindBy(how=How.XPATH,using="//*[@id=\"listing-right\"]/div[2]/p")
    WebElement successmessage;
    @FindBy(how=How.XPATH,using="//*[@id=\"fixed-content-wrapper\"]/p")
    WebElement errormessage;

    public PcGarageMainPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void loginPage(String username, String password) {
        emailField.clear();
        emailField.sendKeys(username);
        passwordField.clear();
        passwordField.sendKeys(password);
        submitButton.submit();
    }

    public void register(String lastname, String firstname, String telephone, String email, String password) {
        newlastname.clear();
        newlastname.sendKeys(lastname);
        newfirstname.clear();
        newfirstname.sendKeys(firstname);
        this.telephone.clear();
        this.telephone.sendKeys(telephone);
        newemail.clear();
        newemail.sendKeys(email);
        newpassword.clear();
        newpassword.sendKeys(password);
        newpasswordretype.clear();
        newpasswordretype.sendKeys(password);
        submitButtonRegister.submit();
    }
    public boolean checkForMessage(String error, String type) {
        if (type.equals("success"))
            return error.equals(successmessage.getText());
        else if (type.equals("error"))
            return error.equals(errormessage.getText());

        return false;
    }
}
