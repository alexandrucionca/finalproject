package pageFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UpdateBasket {
    WebDriver driver;
    WebDriverWait wait;

    public UpdateBasket (WebDriver driver) {
        this.driver=driver;
        PageFactory.initElements(driver, this);
        wait= new WebDriverWait(driver,20);
    }


    @FindBy(how= How.XPATH,using="//*[@id=\"amli_4\"]/a")
    WebElement tab1;
    @FindBy(how= How.XPATH,using="//*[@id=\"ps-shop\"]/div/button")
    WebElement addbutton1;
    @FindBy(how= How.ID,using="searchac")
    WebElement searchtab1;
    @FindBy(how= How.ID,using="sbbt")
    WebElement searchButton1;
    @FindBy (how=How.ID,using="dp_1973998")
    WebElement quantity;
    @FindBy (how= How.XPATH,using="//*[@id=\"cart_form\"]/table/tbody/tr[2]/td[3]/a")
    WebElement clear;
    @FindBy (how= How.XPATH,using="//*[@id=\"cart-page-desktop\"]/div/p[1]/b")
    WebElement clearmessage;
    @FindBy(how=How.XPATH,using="//*[@id=\"cart_form\"]/table/tbody/tr[2]/td[3]/a")
    WebElement actualizare;
    @FindBy(how=How.XPATH,using="\"//td[@class='ct-quantity']\"")
    WebElement nritems;
    public void clearBasket() throws InterruptedException {
        searchtab1.click();
        wait.until(ExpectedConditions.visibilityOf(searchtab1));
        searchtab1.clear();
        searchtab1.sendKeys("Telefon mobil Nokia 3310 3G Dual SIM Charcoal (2017)");
        searchButton1.click();
        wait.until(ExpectedConditions.visibilityOf(addbutton1));
        addbutton1.click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//td[@class='ct-quantity']")));
        int size=driver.findElements(By.xpath("//td[@class='ct-quantity']")).size();
        for (int i=0;i<size;i++) {
            wait.until(ExpectedConditions.visibilityOf(clear));
            clear.click();
        }
        wait.until(ExpectedConditions.visibilityOf(clearmessage));
        String a=clearmessage.getText();
        if (a.equals("Cosul tau de cumparaturi este gol.")){
            System.out.println("Produsul a fost sters");}
            else{
            System.out.println("Produsul nu a putut fi sters");
        }
    }
}
