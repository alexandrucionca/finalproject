package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchFunctionality {
    WebDriver driver;
    WebDriverWait  wait;

    @FindBy(how= How.ID,using="searchac")
    WebElement searchtab;
    @FindBy(how= How.ID,using="sbbt")
    WebElement searchButton;
    //Telefon mobil Nokia 3310 3G Dual SIM Charcoal (2017)
    @FindBy(how= How.XPATH,using="//*[@id=\"container\"]/div[2]/h1")
    WebElement searchedtext;

    public SearchFunctionality (WebDriver driver) {
        this.driver=driver;
        PageFactory.initElements(driver, this);
        wait= new WebDriverWait(driver,20);
    }


    public void searchFunction(String item){
        searchtab.clear();
        searchtab.sendKeys(item);
        searchButton.submit();
        wait.until(ExpectedConditions.visibilityOf(searchedtext));
        System.out.println(searchedtext.getText());
        if (searchedtext.getText().equals(item))
        {
            System.out.println("Cautarea a avut loc cu succes");
         }
        else {
            System.out.println("Cautarea nu a avut loc cu succes");
        }
    }
}
