package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WhishList {
    WebDriver driver;
    WebDriverWait wait;

    public WhishList (WebDriver driver) {
        this.driver=driver;
        PageFactory.initElements(driver, this);
        wait= new WebDriverWait(driver,20);
    }

    @FindBy(how= How.ID,using="searchac")
    WebElement searchtab1;
    @FindBy(how= How.ID,using="sbbt")
    WebElement searchButton1;
    @FindBy(how= How.XPATH,using="//*[@id=\"amli_0\"]/a")
    WebElement tab2;

    @FindBy(how= How.XPATH,using="//*[@id=\"listing-right\"]/div[1]/div/div[1]/div/div[3]/div[2]/a")
    WebElement addbutton2;

    @FindBy(how= How.XPATH,using="//*[@id=\"ps-shop\"]/div/button")
    WebElement addbutton1;
    @FindBy(how= How.XPATH,using="//*[@id=\"cart_form\"]/div[1]/p[3]/a/b")
    WebElement wishbutton;
    @FindBy(how=How.ID,using = "wishlist_name")
    WebElement wishname;
    @FindBy(how=How.ID,using = "customer_comment")
    WebElement customercomment;
    @FindBy(how=How.ID,using = "add_wishlist")
    WebElement addwishbutton;
   // @FindBy(how=How.XPATH,using = "//*[@id=\"add_wishlist\"]/p[2]/text()[1]")
    @FindBy(how=How.XPATH,using="//*[@id=\"listing-right\"]/div[1]/p")
    WebElement wishtext;
    //Puteti vizualiza wishlist-ul creat
    public void createWishList(String name, String comment) throws InterruptedException {
        searchtab1.click();

        searchtab1.clear();
        searchtab1.sendKeys("Telefon mobil Nokia 3310 3G Dual SIM Charcoal (2017)");
        searchButton1.click();
        wait.until(ExpectedConditions.visibilityOf(addbutton1));
        addbutton1.click();
        wait.until(ExpectedConditions.visibilityOf(wishbutton));
        wishbutton.click();
        wishname.clear();
        wishname.sendKeys(name);
        customercomment.clear();
        customercomment.sendKeys(comment);
        addwishbutton.submit();
        wait.until(ExpectedConditions.visibilityOf(wishtext));
        String a=wishtext.getText();

        if (a.equals("Nu puteti introduce acelasi wishlist de mai multe ori.")){
            System.out.println("Wishlist-ul test passed");
        }
        else{
            System.out.println("Wishlist-ul test failed");
        }

    }
}
