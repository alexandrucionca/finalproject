package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddItemToBasket {

    @FindBy (how= How.XPATH,using="//*[@id=\"amli_0\"]/a")
    WebElement tab;

    @FindBy(how= How.XPATH,using="//*[@id=\"listing-right\"]/div[1]/div/div[1]/div/div[3]/div[2]/a")
    WebElement addbutton;

    @FindBy (how= How.XPATH,using="//*[@id=\"cart_form\"]/div[1]/p[3]/a/b")
    WebElement textmessage;
    public AddItemToBasket (WebDriver driver) {
        this.driver=driver;
        PageFactory.initElements(driver, this);
        wait= new WebDriverWait(driver,20);
    }
    WebDriver driver;
    WebDriverWait  wait;

    //Salveaza continutul cosului ca wishlist »
    public void checkBasket() {
        tab.click();
        addbutton.click();
        String a=textmessage.getText();
        if (a.equals("Salveaza continutul cosului ca wishlist »"))
            System.out.println("Produsul a fost adaugat cu succes in cos");
        else
            System.out.println("Produsul nu a fost adaugat cu succes");
    }

}
